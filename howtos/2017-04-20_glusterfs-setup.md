# Setup Gluster FS

## Installation

For Debian:

```
$ sudo apt-get update 
$ sudo apt-get install glusterfs-server glusterfs-common glusterfs-client -y
```

For CentOS:

```
!todo
```

## Working with GlusterFS

This is some text

### Distributed Volume

**Info:**

* Files are randomly distributed over the bricks in distributed volume
* There is no redundancy
* Data loss protection is provided by the underlying hardware. Note: NO Protection from GlusterFS
* Best for scaling size of the Volume

**Setup:**

Setup with 2 Nodes:

```
$ gluster peer probe server1
$ gluster peer probe server2
```

Creating the Volume:

```
$ gluster volume create test-volume server1:/exp1/ server2:/exp2
Creation of volume test-volume has been successful. Please start the volume to access data.
```

Starting the Volume:

```
$ gluster volume start test-volume
```
