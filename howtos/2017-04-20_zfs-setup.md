# Setup ZFS

This tutorial will go through the steps to install ZFS on Ubuntu

## Installation

```
$ sudo apt-get install zfsutils-linux zfsnap -y
```

## Creating Storage Pools

We will create a RAIDZ (1) Volume which is like Raid5 with Single Parity

```
$ zpool create storage-pool raidz1 xvdf xvdg -f
```

## Listing Pools

```
$ zpool list
NAME           SIZE  ALLOC   FREE  EXPANDSZ   FRAG    CAP  DEDUP  HEALTH  ALTROOT
storage-pool   199G   125K   199G         -     0%     0%  1.00x  ONLINE  -
```
